# liquibase

Для запуска нужно:
    1. создать пустую базу store
    2. версия Postgres 9.6
    3. в файле liquibase.properties прописать путь к драйверу
    classpath=c:/Program Files (x86)/PostgreSQL/pgJDBC/postgresql-42.2.2.jar
    4. запуск mvn test (добавил файл setting.xml с возможностью прописать профили для мавена).
    5. организован master.xml , ченджлоги разбиты на инициализацию схемы, добавление триггера, добавление данных
    
  Ограничения:
    1. Максимальный доступный платеж - 500р 
            CREATE OR REPLACE FUNCTION contract_insert_trigger() RETURNS TRIGGER AS $$
            DECLARE
            id INT;
            BEGIN
            IF (TG_OP = 'INSERT') AND ( NEW.amount > 1000)
            THEN
            id = NEW.id;
            INSERT INTO audit(contract_id) VALUES (id);
            ELSE RAISE EXCEPTION 'Date out of range.  Fix the contract_insert_trigger() function!';
            END IF;
            RETURN NULL;
            END;
            $$
            LANGUAGE plpgsql;
    2. При заключении договора на сумму, превышающую 1000р вносить информацию по договору в таблицу аудита
            CREATE OR REPLACE FUNCTION shipment_insert_trigger() RETURNS TRIGGER AS $$
            BEGIN IF ( NEW.location_id >= 1 AND NEW.location_id <= 10)
            THEN INSERT INTO shipment_loc1 VALUES (NEW.*);
            ELSIF ( NEW.location_id >= 11 AND NEW.location_id <= 20)
            THEN INSERT INTO shipment_loc2 VALUES (NEW.*);
            ELSE RAISE EXCEPTION 'Date out of range.  Fix the shipment_insert_trigger() function!';
            END IF;
            RETURN NULL;
            END;
            $$
            LANGUAGE plpgsql;
    3. При организации доставки при количестве товаров больше 10, разбивать на 2 доставки с максимальным вложением 10
                (не успел сделать)


  Отчеты:
    1. Показать всех должников (чей товар доставлен, а сумма по договору не совпадает с суммой по платежам) по договорам с сортировкой по максимальному долгу
                (не успел сделать)
    2. Показать самых лучших клиентов (принесших большую прибыль)
       SELECT o.name, sum(amount) as profit FROM 
            organization o JOIN contract c ON o.id = c.client_id 
            GROUP BY o.name 
            ORDER BY profit DESC
    3. Показать самый популярный продукт доставки и количество за месяц
       SELECT p.name, sum(sp.quantity) as total_qnt from product p
            JOIN shipment_product sp ON p.id = sp.product_id
            Join shipment s ON p.id = sp.shipment_id WHERE extract(MONTH from s.date_fact_delivery)= 5
            GROUP BY p.name ORDER BY total_qnt DESC;
    4. Показать самый популярный город доставки
        SELECT l.city, count(location_id) cnt FROM
            shipment s JOIN location l ON s.location_id = l.id
            GROUP BY l.city ORDER BY cnt DESC;
    5. Показать информацию по самым долгим, в плане доставки, направлениям
        SELECT l.city, max(s.date_fact_delivery - s.date_fact_send) as sendtime FROM
            location l JOIN shipment s ON l.id = s.location_id
            GROUP BY l.city ORDER BY sendtime DESC;