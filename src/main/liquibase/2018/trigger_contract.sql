CREATE OR REPLACE FUNCTION contract_insert_trigger() RETURNS TRIGGER AS $$
DECLARE
id INT;
BEGIN
IF (TG_OP = 'INSERT') AND ( NEW.amount > 1000)
THEN
id = NEW.id;
INSERT INTO audit(contract_id) VALUES (id);
ELSE RAISE EXCEPTION 'Date out of range.  Fix the contract_insert_trigger() function!';
END IF;
RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER insert_contract_trigger
AFTER INSERT ON contract
FOR EACH ROW EXECUTE PROCEDURE contract_insert_trigger();
